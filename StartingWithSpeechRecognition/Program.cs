﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Speech.Recognition;
using System.Speech.Synthesis;
using System.Runtime.InteropServices;

using Newtonsoft.Json;
using System.IO;


namespace SpeechMediaControl
{
    class Program
    {
        public class FileEntry
        {
            public string name { get; set; }
            public bool phonetic { get; set; }
            public string path { get; set; }
        }

        public class RootObject
        {
            public string keyword { get; set; }
            public bool SynthesizeSpeech;
            public List<FileEntry> FileEntries { get; set; }
        }

        RootObject Data;

        SpeechRecognitionEngine _recognizer = null;
        string keyword;
        bool isLaunched = false;
        bool quit = false;
        SpeechSynthesizer speechSynthesizer = new SpeechSynthesizer();

        List<FileEntry> fileEntries;
        static int Main(string[] args)
        {

            new Program().Run();
            return 0;
        }

        void Run()
        {
            ReloadFileEntries();
            LoadSpeechRecognition();
            
            while (!quit)
            {
                Thread.Sleep(1000);
            }

            if (Data.SynthesizeSpeech)
                speechSynthesizer.Speak("Voice recognition stopping");
         
            speechSynthesizer.Dispose();
        }
        
        #region Speech recognition with Choices and GrammarBuilder.Append
        void LoadSpeechRecognition()
        {
            _recognizer = new SpeechRecognitionEngine();
            GrammarBuilder grammarBuilder = new GrammarBuilder();
            if(!string.IsNullOrWhiteSpace(Data.keyword))
                grammarBuilder.Append(Data.keyword);

            //Removed: "exit", "quit",
            var normal = new GrammarBuilder(new Choices(BuildWordList("pause track"), BuildWordList("thank you"), BuildWordList("play track"), BuildWordList("next track"), BuildWordList("restart track"), BuildWordList("previous track")));
            
            var openPrograms = new GrammarBuilder("open");
            openPrograms.Append(BuildProgramsFromEntries());

            var reloading = BuildWordList("reload entries");
            
            grammarBuilder.Append(new Choices(normal,openPrograms, reloading)); 

            _recognizer.RequestRecognizerUpdate();
            var grammar = new Grammar(grammarBuilder);
            _recognizer.LoadGrammar(grammar); // load grammar
            _recognizer.SpeechRecognized += SpeechRecognized;
            _recognizer.SetInputToDefaultAudioDevice(); // set input to default audio device
            _recognizer.RecognizeAsync(RecognizeMode.Multiple); // recognize speech
            Console.WriteLine("Listening for input...");
            
        }

        private Choices BuildProgramsFromEntries()
        {
            Choices c = new Choices();
            foreach(FileEntry entry in fileEntries)
            {
                c.Add(entry.phonetic ? BuildPhonetic(entry.name) : entry.name);
            }
            
            return c;
        }

        GrammarBuilder BuildWordList(params string[] words)
        {
            var grammar = new GrammarBuilder();
            for (int i = 0; i < words.Length; ++i)
            {
                var grammar2 = new GrammarBuilder(words[i].ToString());
                grammar.Append(grammar2);
                grammar2 = grammar;

            }
            return grammar;
        }

        static char[] seperator = new char[] { ' ' }; 
        GrammarBuilder BuildWordList(string words)
        {

            var wordList = words.Split(seperator, StringSplitOptions.RemoveEmptyEntries);
            var grammar = new GrammarBuilder();
            for (int i = 0; i < wordList.Length; ++i)
            {
                var grammar2 = new GrammarBuilder(wordList[i].ToString());
                grammar.Append(grammar2);
                grammar2 = grammar;
                
            }
            return grammar;
        }


        GrammarBuilder BuildPhonetic(string word)
        {
            var keys = word.ToArray();
            var grammar = new GrammarBuilder(keys[0].ToString());
            for (int i = 1; i < keys.Length; ++i)
            {
                grammar.Append(keys[i].ToString());
            }
            return grammar;
        }

        void SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            Console.WriteLine(e.Result.Text);
            string wordtoUse = "";

            if (e.Result.Words[0].Text == keyword && e.Result.Words.Count == 1)
            {
                speechSynthesizer.SpeakAsync("Yes sir?");
                return;
            }

            if (String.IsNullOrWhiteSpace(keyword))
                wordtoUse = e.Result.Words[0].Text;
            else
                wordtoUse = e.Result.Words[1].Text;

            //e.Result.Alternates.ToList().ForEach(alt => Console.WriteLine(alt.Text)) ;

            if (e.Result.Text.Contains("thank you"))
            {
                speechSynthesizer.SpeakAsync("No problem sir");
                return;
            }

            switch (wordtoUse)
            {
                case "quit":
                    quit = true;
                    break;
                case "play":
                    MessagePump.SendMessage(SystemMessages.MEDIA_PLAY_PAUSE);
                    //Keyboard.KeyPress(VirtualKeyCode.MEDIA_PLAY_PAUSE);
                    break;
                case "pause":
                    MessagePump.SendMessage(SystemMessages.MEDIA_PLAY_PAUSE);
                    //Keyboard.KeyPress(VirtualKeyCode.MEDIA_PLAY_PAUSE);
                    break;
                case "stop":
                    MessagePump.SendMessage(SystemMessages.MEDIA_STOP);
                    break;
                case "next":
                    MessagePump.SendMessage(SystemMessages.MEDIA_NEXTTRACK);
                    break;
                case "restart":
                case "back":
                    MessagePump.SendMessage(SystemMessages.MEDIA_PREVIOUSTRACK);
                    break;
                case "previous":
                    MessagePump.SendMessage(SystemMessages.MEDIA_PREVIOUSTRACK);
                    MessagePump.SendMessage(SystemMessages.MEDIA_PREVIOUSTRACK);
                    break;
                case "left":
                    Keyboard.KeyPress(VirtualKeyCode.LEFT);
                    break;
                case "right":
                    Keyboard.KeyPress(VirtualKeyCode.RIGHT);
                    break;
                case "open":
                    if(Data.SynthesizeSpeech)
                        speechSynthesizer.SpeakAsync("Opening program");
                    HandleOpen(e.Result.Words.Skip(2).ToList());
                    break;
                case "reload":
                    ReloadFileEntries();
                    break;
            }
            
            
        }

        private void HandleOpen(List<RecognizedWordUnit> words)
        {
            StringBuilder sb = new StringBuilder();
            words.ForEach(w => sb.Append(w.Text));
            var name = sb.ToString();
            var entry = fileEntries.ToList().Find(file => file.name == name);
            try { 
                System.Diagnostics.Process.Start(entry.path); 
            }
            catch(Exception e)
            {
                Console.WriteLine("Can not find program \""+name +"\"");
            }
            
        }

        private void ReloadFileEntries()
        {
            Console.WriteLine("Loading json");

           /* var parsed = JsonObject.Parse(File.OpenText("speechconfig.json").ReadToEnd());
            var keyword = parsed.SelectToken("keyword").Values().First().ToString();*/

            var serializer = new JsonSerializer();
            var reader = new JsonTextReader(File.OpenText("speechconfig.json"));
            Data = serializer.Deserialize<RootObject>(reader);
            keyword = Data.keyword;
            fileEntries = Data.FileEntries.ToList();
            reader.Close();
            
        }

        #endregion
    }
}

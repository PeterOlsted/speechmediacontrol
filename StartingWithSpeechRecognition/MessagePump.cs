﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace SpeechMediaControl
{
    public enum SystemMessages
        {

            MEDIA_NEXTTRACK     =  11,
            MEDIA_PREVIOUSTRACK =  12,
            MEDIA_STOP          =  13,
            MEDIA_PLAY_PAUSE    =  14

        }

    class MessagePump
    {
        [DllImport("User32.DLL")]
        public static extern IntPtr SendMessage(int hWnd, int Msg, int wParam, int lParam);


        private const int WM_APPCOMMAND = 0x319;

        public static void SendMessage(SystemMessages message)
        {
            // Start the screen saver.
            //SendMessage(Process.GetCurrentProcess().MainWindowHandle, WM_SYSCOMMAND, SC_SCREENSAVE, 0);
            SendMessage((int)Process.GetCurrentProcess().MainWindowHandle, WM_APPCOMMAND, (int)Process.GetCurrentProcess().MainWindowHandle,
            ((int)message) << 16);
        }
    }
}

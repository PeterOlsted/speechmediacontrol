﻿using System.Runtime.InteropServices;
using System.Threading;

namespace SpeechMediaControl
{
    enum VirtualKeyCode{
        LEFT = 0x25,
        RIGHT = 0x27,
        MEDIA_NEXT_TRACK = 0xB0,
        MEDIA_PREV_TRACK = 0xB1,
        MEDIA_STOP = 0xB2,
        MEDIA_PLAY_PAUSE = 0xB3,
    }

    class Keyboard
    {
        const int KEYEVENTF_EXTENDEDKEY = 0x1;
        const int KEYEVENTF_KEYUP = 0x2;

        [DllImport("user32.dll")]
        static extern void keybd_event(byte key, byte scan, int flags, int extraInfo);

        public static void KeyDown(VirtualKeyCode key)
        {
            keybd_event((byte)key, 0, 0, 0);
        }

        public static void KeyUp(VirtualKeyCode key)
        {
            keybd_event((byte)(key), 0, KEYEVENTF_KEYUP, 0);
        }

        public static void KeyPress(VirtualKeyCode key)
        {
            KeyDown(key);
            KeyUp(key);
        }

        /*[DllImport("user32.dll")]
        public static extern void keybd_event  
        (
            byte bVk,
            byte bScan,
            uint dwFlags,
            uint dwExtraInfo
        );
*/

        /*public static void KeyPress(VirtualKeyCode key)
        {
            Keyboard.KeyDown(key);
            Thread.Sleep(50);
            Keyboard.KeyUp(key); 
        }

        public static void KeyDown(VirtualKeyCode key)
        {
            keybd_event((byte)key, 0, 0, 0);
        }

        public static void KeyUp(VirtualKeyCode key)
        {
            keybd_event((byte)key, 0, 2, 0);
        }*/
    }
}